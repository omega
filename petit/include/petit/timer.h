/* $Id: timer.h,v 1.1.1.1 2000/06/29 19:24:28 dwonnaco Exp $ */
#ifndef Already_Included_Timer
#define Already_Included_Timer

void start_clock( void );
long clock_diff( void );  /* return user time since start_clock */

#endif

